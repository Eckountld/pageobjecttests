package Business_flow;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class AEM_feature_methods {

    @Steps
    step_Methods steps = new step_Methods();


    @When("^We open the chromeDriver$")
    public void we_open_the_chromeDriver() throws Throwable {
         steps.openSite("http://www.google.com");
    }

    @Then("^search AEM$")
    public void search_AEM() throws Throwable {
        steps.search("q", "AEM");
    }

    @Then("^we return all the links$")
    public void we_return_all_the_links() throws Throwable {
        steps.getLinks("AEM");
    }

    @Then("^close the driver$")
    public void close_the_driver() throws Throwable {
          steps.close();
    }


}
