package Business_flow;

import Technical.components;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;


public class step_Methods {


    @Managed(driver="chrome")
    WebDriver browser;

    WebElement searchbar;

    List<WebElement> links = new ArrayList<WebElement>();
    components driverclass = new components(browser);


    //Opens the ChromDriver and opens the link tht is required
    @Step
    public void openSite(String Site)
    {
        driverclass.setDriver(Site);
    }

    //Submits the string required in the searchox
    @Step
    public void search(String Element, String Keys)
    {
        searchbar = driverclass.findByName(Element);
        searchbar.sendKeys(Keys);
        searchbar.submit();
    }


    //Gets all the links on the page and see if the on searched for exists
    @Step
    public void getLinks(String wordSearch)
    {
        links = driverclass.getAllLinks("a");

        for(WebElement link:links)
        {
              if(link.getText().contains(wordSearch))
              {
                  System.out.println("Does Exist");
                  break;
              }
        }
    }

    //Closes the Driver after completion
    @Step
    public void close()
    {
        driverclass.closeDriver();

    }
}
