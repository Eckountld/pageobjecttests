package Business_flow;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class BDD_feature_Methods {

             @Steps
            step_Methods steps = new step_Methods();

    @When("^We open the driver$")
    public void we_open_the_driver() throws Throwable {
        steps.openSite("https://www.wikipedia.org/");
    }

    @When("^search for Behaviour Driven Development$")
    public void search_for_Behaviour_Driven_Development() throws Throwable {
        steps.search("search","Behaviour Driven Development");
    }

    @Then("^Get all the links on the page$")
    public void get_all_the_links_on_the_page() throws Throwable {
        steps.getLinks("BDD");
    }

    @Then("^Close the driver$")
    public void close_the_driver() throws Throwable {
            steps.close();
    }


}
