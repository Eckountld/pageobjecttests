package Technical;


import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;


public class components extends PageObject{
          WebDriver driver;
    public components(WebDriver driver) {
                 super(driver);
    }

    private List<WebElement> links = new ArrayList();
    public void setDriver(String Site)
    {
        try {
            System.setProperty("webdriver.chrome.driver","C:\\repositories\\pageobjecttests\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.get(Site);
        }catch (NullPointerException np){
            System.out.println(np.getMessage());}
    }

    public WebElement findByName(String request)
    {
       WebElement element = driver.findElement(By.name(request));
           return element;
    }

    public WebElement findById(String request)
    {
       WebElement element = driver.findElement(By.id(request));
        return element;
    }
    public List<WebElement> getAllLinks(String request)
    {
        links = driver.findElements(By.tagName(request));
        return links;
    }


    public void closeDriver()
    {
        driver.close();
    }

}

